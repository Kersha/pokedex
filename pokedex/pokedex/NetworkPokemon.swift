//
//  NetworkPokemon.swift
//  pokedex
//
//  Created by carlos on 13/6/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import Foundation
import Alamofire
// alamofire instalacion
// instalar cocoapods
// en la carpeta del proyecto poner pod init
// en el archivo Podfile añadir una line (ver documentacion)
// ejecturar pod install
// importar Alamofire

// abrir workspace no project
// ejectura command + B para construir todo
class NetworkPokemon{
    func getAllPokemon(completion:@escaping(([Pokemon])->())){
        var pokemonArrary:[Pokemon] = []
        // no usar completion para devolver varios request no espera
        // DispatchGroup funciona como promesa
        let group = DispatchGroup()
        for i in 1...8{
            //empece algo
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON
                { response in
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                
                //print(pokemon)
                pokemonArrary.append(pokemon)
                //le digo que termine
                group.leave()
            }
        }
        // espera que todos los group enter hayan finalizado
        // main es un hilo principal
        group.notify(queue: .main) {
            
            // aqui lo que se hace al terminar todos los group
           completion(pokemonArrary)
        }
        
    }
    
    
    func getPokemon(number:Int,completion:@escaping(([Pokemon])->())){
        let group = DispatchGroup()
        var pokemonR:[Pokemon] = []
        group.enter()
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(number)").responseJSON
            { response in
                guard let data = response.data else {
                    print("Error")
                    return
                }
                
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                
                print(pokemon)
                //le digo que termine
                pokemonR.append(pokemon)
                group.leave()
        }
        group.notify(queue: .main) {
            
            // aqui lo que se hace al terminar todos los group
            completion(pokemonR)
        }
    }
    
    func getPokemonImage(url:String){
        
    }
}
