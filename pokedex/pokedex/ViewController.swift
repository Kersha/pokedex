//
//  ViewController.swift
//  pokedex
//
//  Created by carlos on 13/6/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet weak var tablaPokemon: UITableView!
    
    var pokemons:[Pokemon] = []
    var pokeNombre = ""
    var pokePeso = ""
    var pokeAltura = ""
    var urlImg = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let networkPokemon=NetworkPokemon()
        // rescato el completion
        networkPokemon.getAllPokemon{(pokemonArr)in
            self.pokemons = pokemonArr
            self.tablaPokemon.reloadData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Siempre vincular la table view con el view controller(ctrl de la tabla al controller y seleccionar source y delegate)
    // Poner el identificador de la celda y asignarle una funcion tableViewCell
    // funciones para configurar y llenar tabla
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = pokemons[indexPath.row].name
        return  cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    // esta funcion da funcionalidad cuando se da clic una celda
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pokeNombre = pokemons[indexPath.row].name
        pokePeso = String(pokemons[indexPath.row].weight)
        pokeAltura = String(pokemons[indexPath.row].height)
        urlImg = pokemons[indexPath.row].sprites.defaultSprite
        
        performSegue(withIdentifier: "pantallaDos", sender: self)
    }


    // funcion para pasar parametros
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "pantallaDos"{
            let destinationController = segue.destination as! ViewController2
            destinationController.pokeNombre = pokeNombre
            destinationController.pokePeso = pokePeso
            destinationController.pokeAltura = pokeAltura
            destinationController.urlImg = urlImg
        }
    }

}

