//
//  ViewController2.swift
//  pokedex
//
//  Created by carlos on 20/6/18.
//  Copyright © 2018 carlos. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var txtPeso: UILabel!
    @IBOutlet weak var txtAltura: UILabel!
    @IBOutlet weak var txtNombre: UILabel!
    
    @IBOutlet weak var imagenView: UIImageView!
    var pokeNombre : String?
    var pokeAltura : String?
    var pokePeso : String?
    var urlImg: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtNombre.text = pokeNombre
        txtPeso.text = pokePeso
        txtAltura.text = pokeAltura
        get_Img(urlImg!, imagenView)
        //imagenView.
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func get_Img(_ url_str:String,_ imageView:UIImageView){
        let url:URL = URL(string: url_str)!
        let session = URLSession.shared
        let task = session.dataTask(with: url,completionHandler: { (data, response, error) in
            if data != nil{
                let image = UIImage(data:data!)
                if(image != nil){
                    DispatchQueue.main.async(execute: {imageView.image=image
                            
                        })
                    }
                }
        })
        task.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
